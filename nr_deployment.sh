#!/bin/bash


#New Relic
revision=$(ssh oht@107.23.229.181 "ssh oht@puppet \"cd /home/oht/public_html/oht; git describe --tags --abbrev=0\"")
api_key="x-api-key:5d5e02e47b38aa4c97825eea2a583fac9d55347e9e902e3"
app_id=1895760
desc="OHT deployed"
user=${USER^}
url="https://api.newrelic.com/deployments.xml"

curl -H "$api_key" -d "deployment[application_id]=$app_id" -d "deployment[description]=$desc" -d "deployment[revision]=$revision" -d "deployment[user]=$user " $url

# HipChat
color="green"
from="Capistrano"
message="@all Revision $revision deployed to One Hour Translation by $user"
room="104371"
token="uDGHu1YN3s5al0LWBNGVXi77lxSRFtMQ8IuNp37Q"
hipchat_url="https://api.hipchat.com/v2/room/$room/notification?auth_token=$token"

curl -sS -H 'Content-type: application/json' -d "{\"color\":\"$color\", \"message\":\"$message\", \"message_format\":\"text\", \"from\":\"$from\", \"notify\":true}" $hipchat_url

# SLACK

#url's VOVA
#slack_url_1="https://hooks.slack.com/services/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"
#slack_url_2="https://hooks.slack.com/services/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"
#slack_url_3="https://hooks.slack.com/services/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"

#url's YONI
slack_url_1="https://hooks.slack.com/services/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"
slack_url_2="https://hooks.slack.com/services/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"
slack_url_3="https://hooks.slack.com/services/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"



#Text
slack_text="<!channel>: New OHT revision $revision deployed to production by $user"
slack_text_3="<!channel>: Our highly skilled R&D team just deployed the new OHT revision $revision to production!!! Hooray!!! :tada:"

#text for message
slack_text_message="<!channel>: New OHT revision $revision deployed by $user:"
slack_text_message_3="<!channel>: Our highly skilled R&D team just deployed OHT revision $revision: "
#Channels
slack_channel_1="#rnd-ohtdeploy"
slack_channel_2="#rnd-ninjas"
slack_channel_3="#oht-announcements"

slack_channel_4="#cap_test"


slack_test="a few words and:"
#curl -X POST --data-urlencode "payload={\"channel\": \"$slack_channel_1\", \"username\": \"$from\", \"text\": \"$slack_text\", \"icon_emoji\": \":capistrano:\"}" $slack_url_1
#curl -X POST --data-urlencode "payload={\"channel\": \"$slack_channel_2\", \"username\": \"$from\", \"text\": \"$slack_text\", \"icon_emoji\": \":capistrano:\"}" $slack_url_2
#curl -X POST --data-urlencode "payload={\"channel\": \"$slack_channel_3\", \"username\": \"$from\", \"text\": \"$slack_text_3\", \"icon_emoji\": \":oht-logo:\"}" $slack_url_3

#echo $1
#echo $2

#if [ "$1" -eq "1" ]
#then
#curl -X POST --data-urlencode "payload={\"channel\": \"$slack_channel_3\", \"username\": \"$from\", \"text\": \"$2\", \"icon_emoji\": \":oht-logo:\"}" $slack_url_3
#else
#curl -X POST --data-urlencode "payload={\"channel\": \"$slack_channel_3\", \"username\": \"$from\", \"text\": \"default\", \"icon_emoji\": \":oht-logo:\"}" $slack_url_3
#fi

if [ -z "$1" ]
then
echo "no flag given"
#curl -X POST --data-urlencode "payload={\"channel\": \"$slack_channel_4\", \"username\": \"$from\", \"text\": \"default\", \"icon_emoji\": \":oht-logo:\"}" $slack_url_3

curl -X POST --data-urlencode "payload={\"channel\": \"$slack_channel_1\", \"username\": \"$from\", \"text\": \"$slack_text\", \"icon_emoji\": \":capistrano:\"}" $slack_url_1
curl -X POST --data-urlencode "payload={\"channel\": \"$slack_channel_2\", \"username\": \"$from\", \"text\": \"$slack_text\", \"icon_emoji\": \":capistrano:\"}" $slack_url_2
curl -X POST --data-urlencode "payload={\"channel\": \"$slack_channel_3\", \"username\": \"$from\", \"text\": \"$slack_text_3\", \"icon_emoji\": \":oht-logo:\"}" $slack_url_3

else
echo "flag is "$1""
#curl -X POST --data-urlencode "payload={\"channel\": \"$slack_channel_4\", \"username\": \"$from\", \"text\": \"$slack_test $1\", \"icon_emoji\": \":oht-logo:\"}" $slack_url_3

curl -X POST --data-urlencode "payload={\"channel\": \"$slack_channel_1\", \"username\": \"$from\", \"text\": \"$slack_text_message $1\", \"icon_emoji\": \":capistrano:\"}" $slack_url_1
curl -X POST --data-urlencode "payload={\"channel\": \"$slack_channel_2\", \"username\": \"$from\", \"text\": \"$slack_text_message $1\", \"icon_emoji\": \":capistrano:\"}" $slack_url_2
curl -X POST --data-urlencode "payload={\"channel\": \"$slack_channel_3\", \"username\": \"$from\", \"text\": \"$slack_text_message_3 $1\", \"icon_emoji\": \":oht-logo:\"}" $slack_url_3



fi
