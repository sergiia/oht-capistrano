set :puppet_repository, "git@bitbucket.org:oht-it/puppet.git"

###### Puppetmaster ########
 
namespace :puppetmaster do
   desc "Check certificates list"
      task :list_certificates, :roles => :puppetmaster do
        puts "Checking certificates on puppetmaster Virginia"
        run "#{sudo} puppet cert list --all"
      end    
     
   desc "Sign all waiting certificates"
      task :sign_certificates, :roles => :puppetmaster do
        puts "Signing all waiting certificates..."
        run "#{sudo} puppet cert  sign --all || true"
      end
      
  desc "Pull puppet modules from git repository"
       task :git_pull, :roles => :puppetmaster do
         puts "Executing git pull"
         run "cd /etc/puppet; #{sudo} git config --replace-all remote.origin.url #{puppet_repository}; #{sudo} git pull"
	 run "#{sudo} service apache2 restart"
       end      
       
   end    

  
##### Puppets #####
  
namespace :puppet do

##### Frontend ##### 
    
  namespace :frontend do       
         desc "Run puppet agent on frontend servers"
         task :puppet_run , :roles =>  :frontend_VPC do
           puts "Executing puppet agent"
           run "#{sudo} puppet agent -vt || true"
         end
    desc "Test puppet run on frontend"
       task :puppet_test, :roles => :frontend_VPC do
        puts "Puppet agent test run"
         run "#{sudo} puppet agent -vt --noop || true"
      end        
   end 
              
##### Forum/Old blog #####
   
  namespace :forum do       
       desc "Run puppet agent on Forum/Old blog server"
       task :puppet_run , :roles => :forum do
         puts "Executing puppet agent"
         run "#{sudo} puppet agent -vt || true"
       end
       
       desc "Test puppet run on Forum/Old blog server"
          task :puppet_test, :roles => :forum do
           puts "Puppet agent test run"
            run "#{sudo} puppet agent -vt --noop || true"
          end
     end  
   
##### Drupal ##### 
   namespace :drupal do   
     desc "Run puppet agent on drupal server"
     task :puppet_run, :roles =>  :drupal_server do
       puts "Executing puppet agent"
       run "#{sudo} puppet agent -vt || true"
     end
     
     desc "Test puppet run on drupal"
        task :puppet_test, :roles =>  :drupal_server do
         puts "Puppet agent test run"
          run "#{sudo} puppet agent -vt --noop || true"
        end
   end

##### Backend ##### 
   
   namespace :backend do
     desc "Run puppet agent on backend"
     task :puppet_run , :roles => [ :backend, :sweatshop ] do
       puts "Executing puppet agent"
       run "#{sudo} puppet agent -vt || true"
       run "killall php"
     end
     desc "Test puppet run on backend"
        task :puppet_test , :roles => [ :backend, :sweatshop ] do
          puts "Executing puppet agent test run (--noop)"
          run "#{sudo} puppet agent -vt --noop || true"
        end     
   end

  ##### Sandbox Server #####
  
     namespace :sandbox do
        desc "Run puppet agent on Sandbox VPC server"
        task :puppet_run, :roles => :sandbox do
          puts "Executing puppet agent"
          run "#{sudo} puppet agent -vt || true"
	  run "killall php"
        end 
          
           desc "Run puppet test on Sandbox VPC server"
           task :puppet_test, :roles => :sandbox do
             puts "Executing puppet agent test run (--noop)"
             run "#{sudo} puppet agent -vt --noop || true"
           end  
      
      end      
   
   
##### Frontend, Drupal, Forum, Sandbox, Sweatshop and Backend #####   
      
  namespace :webservers do
    desc "Run puppet agent on frontend, Drupal, Forum, backend and Sandbox servers"    
    task :puppet_run do 
      puts "Executing puppet agent"
      frontend.puppet_run
      backend.puppet_run
      drupal.puppet_run
      forum.puppet_run
      sandbox.puppet_run
    end
    
    desc "Test puppet run on frontend, Drupal, Forum, backend and Sandbox servers"
    task :puppet_test do
      puts "Executing puppet agent test run (--noop)"
      frontend.puppet_test
      backend.puppet_test
      drupal.puppet_test
      forum.puppet_test
      sandbox.puppet_test

    end     
    
  end
            
##### Production (Frontend, Drupal, Sweatshop, Backend and Sandbox) #####   
      
  namespace :production do
    desc "Run puppet agent on frontend, Drupal, backend servers"    
    task :puppet_run do 
      puts "Executing puppet agent"
      frontend.puppet_run
      backend.puppet_run
      drupal.puppet_run
      sandbox.puppet_run
    end
    
    desc "Test puppet run on frontend, Drupal, backend servers"
    task :puppet_test do
      puts "Executing puppet agent test run (--noop)"
      frontend.puppet_test
      backend.puppet_test
      drupal.puppet_test
      sandbox.puppet_test
    end     
    
  end
 

##### Tester ##### 
      
    namespace :tester do  
      
      desc "Run puppet agent on tester server"
      task :puppet_run, :roles => :tester do
        puts "Executing puppet agent"
        run "#{sudo} puppet agent -vt || true"
      end
    end
        
##### Mongo servers #####
    
  namespace :mongo do   
      
    desc "Run puppet agent on mongo servers (master+slave)"
          task :puppet_run , :roles => :mongo do
            puts "Executing puppet agent"
            run "#{sudo} puppet agent -vt || true"
          end       
    desc "Run puppet test on mongo servers (master+slave)"
         task :puppet_test, :roles => :mongo do
              puts "Executing puppet agent test run (--noop)"
              run "#{sudo} puppet agent -vt --noop || true"
           end       
           
    end 
##### Mongo backup servers #####
namespace :mongo_backup do   
     
  desc "Run puppet agent on mongo-backup servers"
  task :puppet_run , :roles => :mongo,  :only => { :backup => true } do
    puts "Executing puppet agent"
    run "#{sudo} puppet agent -vt || true"
  end   
end 

##### Nameservers  #####
namespace :nameserver do
  
  desc "Run puppet agent on nameservers"
  task :puppet_run, :roles => :nameserver do
    puts "Executing puppet agent"
    run "#{sudo} puppet agent -vt || true"
  end
  
       
     desc "Run puppet test on nameservers"
     task :puppet_test, :roles => :nameserver do
       puts "Executing puppet test run"
       run "#{sudo} puppet agent -vt --noop || true"
     end

end

##### MySQL Percona Servers #####

  namespace :mysql_percona do
     desc "Run puppet agent on percona servers"
     task :puppet_run, :roles => :mysql do
          puts "Executing puppet agent"
          run "#{sudo} puppet agent -vt || true"
        end
     desc "Run puppet test on percona servers"
     task :puppet_test, :roles => :mysql do
          puts "Executing puppet agent test run (--noop)"
          run "#{sudo} puppet agent -vt --noop || true"
        end
   end
 
### Slave + Backup ###
   
  namespace :mysql_percona_slaves do
   desc "Run puppet agent on percona slaves"
   task :puppet_run, :roles => :mysql, :only => { :slave => true } do
        puts "Executing puppet agent"
        run "#{sudo} puppet agent -vt || true"
      end
   desc "Run puppet test on percona slaves"
   task :puppet_test, :roles => :mysql, :only => { :slave => true } do
        puts "Executing puppet agent test run (--noop)"
        run "#{sudo} puppet agent -vt --noop || true"
      end
 end
   
 ##### MySQL backup servers #####
 
  namespace :mysql_backup do
     
     desc "Run puppet agent on MySQL Backup servers"
     task :puppet_run, :roles => :mysql, :only => { :backup => true }  do
       puts "Executing puppet agent"
       run "#{sudo} puppet agent -vt || true"
     end
    end  
        
##### OTRS Server #####
    
    namespace :otrs do
      desc "Run puppet agent on OTRS server"
      task :puppet_run, :roles => :otrs do
           puts "Executing puppet agent"
           run "#{sudo} puppet agent -vt || true"
         end
      desc "Run puppet test on OTRS server"
      task :puppet_test, :roles => :otrs do
           puts "Executing puppet agent test run (--noop)"
           run "#{sudo} puppet agent -vt --noop || true"
         end
    end

##### Memcached Server #####
   
 namespace :memcached do
   desc "Run puppet agent on Memcached server"
   task :puppet_run, :roles => :memcached do
     puts "Executing puppet agent"
     run "#{sudo} puppet agent -vt || true"
   end 
     
      desc "Run puppet test on Memcached server"
      task :puppet_test, :roles => :memcached do
        puts "Executing puppet agent test run (--noop)"
        run "#{sudo} puppet agent -vt --noop || true"
      end  
 
 end 

##### TM Server #####

   namespace :tmserver do
      desc "Run puppet agent on TM Server server"
      task :puppet_run, :roles => :tmserver do
        puts "Executing puppet agent"
        run "#{sudo} puppet agent -vt || true"
      end 
        
         desc "Run puppet test on TM Server server"
         task :puppet_test, :roles => :tmserver do
           puts "Executing puppet agent test run (--noop)"
           run "#{sudo} puppet agent -vt --noop || true"
         end  
    
    end     


            
end 

      
##### Frontend, Drupal, Mongos and Backend remove hosts ##### 
 namespace :puppets do   
   desc "Remove known_hosts file (if puppetmaster was changed)"
   task :remove_known_hosts, :roles => [:drupal_server, 
                                        :frontend_VPC, 
                                        :backend,
                                        :sweatshop,
                                        :mongo, 
                                        :mongo_backup, 
                                        :memcached, 
                                        :nameserver, 
                                         
                                        :mysql,
                                        ] do
     puts "Removing /home/oht/.ssh/known_hosts and puppet SSL certificates"
     run "rm /home/oht/.ssh/known_hosts || true"
     run "#{sudo} rm -rf /var/lib/puppet/ssl/"
    # run "#{sudo} rm /root/.ssh/known_hosts"
   end  
  
########### Run all puppetized nodes ###########   
  
   desc <<-DESC
   Run puppet agent on instances:             
      
      *Frontend, Drupal, Backend servers
      *Mongo master, replicas and backup
      *Mongo master and replicas          
      *Memcached
      *Percona Slaves and backup in 3 regions    
      *Nameservers
      *OTRS
      *TM Server
DESC
   
task :puppet_run do 
     
     puppet.webservers.puppet_run  # www1-5, drupal, backend
     puppet.mongo.puppet_run
     puppet.mongo_backup.puppet_run
     puppet.nameserver.puppet_run
     puppet.mysql_percona_slaves.puppet_run
     puppet.mysql_backup.puppet_run
     puppet.otrs.puppet_run
     puppet.memcached.puppet_run
     puppet.tmserver.puppet_run
     
     puts "Run puppet agent on all nodes"
     run "#{sudo} puppet agent -vt || true"
   end

   ########### Test run on all puppetized nodes ###########   
     
      desc <<-DESC
      Test puppet run on instances:             
         
         *Frontend, Drupal, Backend servers
         *Mongo master, replicas and backup          
         *Memcached
         *Percona Slaves and backup in 3 regions    
         *Nameservers
         *OTRS
         *TM Server
DESC
      
   task :puppet_test do 
        
        puppet.webservers.puppet_test  # Frontends, drupal, backend
        puppet.mongo.puppet_test
        puppet.mongo_backup.puppet_test
        puppet.nameserver.puppet_test
        puppet.mysql_percona_slaves.puppet_test
        puppet.mysql_backup.puppet_test
        puppet.otrs.puppet_test
        puppet.memcached.puppet_test
        puppet.tmserver.puppet_test
        
        puts "Run puppet test on all nodes"
        run "#{sudo} puppet agent -vt --noop || true"
      end
   
      
  end
  

