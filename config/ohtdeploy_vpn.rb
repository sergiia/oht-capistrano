set :application, "oht"
set :repository, "git@bitbucket.org:ohtrnd/oht.git"
#set :repo_west, "git@bitbucket.org:ohtrnd/west.git"
#set :repo_drupal, "git@bitbucket.org:ohtrnd/drupal.git"
#set :repository_test, "git@bitbucket.org:vladimir_oht/oht-test.git"
set :oht_home, "/home/oht/public_html/oht"
#set :west_home, "/home/oht/public_html/west"
#set :drupal_home, "/home/oht/public_html/drupal"
#set :backend_home, "/home/oht/applications/oht" 
#set :repository, "clone oht@puppet.oht.useast:/mnt/repositories/oht/oht.git" # Backup repository
#set :gateway, '107.23.229.181' 
#set :user , "oht"

default_run_options[:pty] = true 

#role :api, "10.0.0.10"
#role :updater, "10.0.1.222"
#role :www1, "10.0.2.10"
#role :www_test, "10.0.10.10"
##AutoScalingGroups
#role :frontend_VPC, "10.0.2.10", "10.0.2.11",  "10.0.2.12" , "10.0.2.13" , "10.0.2.14" , "10.0.2.15"  , "10.0.2.16"#, "10.0.10.10"#, "10.0.2.16"
#role :drupal_server, "10.0.2.100" 
#role :memcached, "10.0.1.200"


namespace :oht do

  #########   Frontend (www) servers #######
  namespace :frontend do
    
    desc "Update frontend servers from Git repository"
    task :git_update, :roles=> :frontend_VPC do
      puts "Updating frontend"      
      run "cd #{oht_home}; git config --replace-all remote.origin.url #{repository}; git checkout master; git reset --hard; git pull; git submodule update; composer install"
      run "#{sudo} rm -rf /tmp/twig_cache"
    end

   desc "Update frontend servers WEST repository"
    task :git_update_WEST, :roles=> :frontend_VPC do
      puts "Updating WEST on frontends"
      run "cd #{west_home}; git config --replace-all remote.origin.url #{repo_west}; git checkout master; git reset --hard; git pull; git submodule update;"
    end
    
    desc "Update frontend servers from Git repository and build frontend pages"
    task :git_update_build, :roles=> :frontend_VPC do
      puts "Updating frontend"
      run "cd #{oht_home}; git config --replace-all remote.origin.url #{repository}; git checkout master; git reset --hard; git pull; git submodule update; composer install"
      run "chmod +x #{oht_home}/scripts/build_frontend_pages.sh"
      run "#{oht_home}/scripts/build_frontend_pages.sh"
      run "#{sudo} rm -rf /tmp/twig_cache"
    end

    desc "Service apache2 restart (www servers)"
    task :frontends_apache_restart, :roles => :frontend_VPC do
      run "#{sudo} service apache2 restart"
    end  
    
    desc "Service apache2 restart (drupal server)"  
    task :drupal_apache_restart, :roles => :drupal_server do 
      run "#{sudo} service apache2 restart"
    end
            
  end
  
  ######## Update Tracker server ########
    
    desc "Update Tracker from Git repository"
    task :tracker_update, :roles => :tracker do
      puts "Updating Tracker server"
      run "cd /home/oht/public_html/tracker-py; git checkout master; git pull"
      run "#{sudo} service apache2 restart"
    end     
  
    
  ######## Update TM Server ########
    
    desc "Update TM Server from Git repository"
    task :tmserver_update, :roles => [ :tmserver, :tmserver_sandbox ] do
      puts "Updating TM Servers"
      run "cd /opt/tmserver; git checkout master ; git pull"
      run "#{sudo} service lighttpd restart"
    end
    
  ######## Backend server #######
  
  namespace :backend do
    
    desc "Update backend from Git repository"
    task :git_update, :roles => :backend do
      puts "Updating backend"      
      run "cd #{backend_home}; git config --replace-all remote.origin.url #{repository}; git checkout master; git reset --hard; git pull; git submodule update; composer install" 
      run "#{sudo} rm -rf /tmp/twig_cache"
      run "#{sudo} /opt/scripts/cron_builder.sh"
      run "killall php"
    end    
      
  end

  ######## Backend server #######

  namespace :backend2 do

    desc "git update and cron builder"
    task :git_update_build_cron, :roles => :backend2 do
      puts "Updating backend2"
      run "cd #{backend_home}; git config --replace-all remote.origin.url #{repository}; git checkout master; git reset --hard; git pull; git submodule update; composer install"
      run "#{sudo} rm -rf /tmp/twig_cache"
      run "#{sudo} /opt/scripts/cron_builder.sh"
      run "killall php"
    end

  end
  
  ######## Sweatshop server #######
   
   namespace :sweatshop do
     
     desc "Update sweatshop from Git repository"
     task :git_update, :roles => :sweatshop do
       puts "Updating sweatshop"       
       run "cd #{backend_home}; git config --replace-all remote.origin.url #{repository}; git checkout master; git reset --hard; git pull; git submodule update; composer install" 
      run "#{sudo} rm -rf /tmp/twig_cache"
      run "killall -q php; true"
     end    
       
   end  

 ###### Versions ######

   namespace :version do 

      desc "Update OHT Version (patch)"
      task :patch, :roles => :updater do
        puts "Updating patch version"
	run "cd #{oht_home}; git config --replace-all remote.origin.url #{repository}; git checkout master; git fetch origin"
	local_sha = capture("cd #{oht_home}; git rev-parse master")
	remote_sha = capture("cd #{oht_home}; git rev-parse origin/master")
	if local_sha != remote_sha
          run "cd #{oht_home}; git pull; /opt/scripts/oht_version.sh --patch"
	else
	  puts "No changes - no tags!"
	end
      end
    
      desc "Update OHT Version (minor)"
      task :minor, :roles => :updater do
        puts "Updating minor version"
	run "cd #{oht_home}; git config --replace-all remote.origin.url #{repository}; git checkout master ; git fetch origin"
        local_sha = capture("cd #{oht_home}; git rev-parse master")
        remote_sha = capture("cd #{oht_home}; git rev-parse origin/master")
        if local_sha != remote_sha
        run "cd #{oht_home}; git pull; /opt/scripts/oht_version.sh --minor"
        else
          puts "No changes - no tags!"
	end
      end
      
      desc "Update OHT Version (major)"
      task :major, :roles => :updater do
        puts "Updating major version"
        run "cd #{oht_home}; git config --replace-all remote.origin.url #{repository}; git checkout master ; git fetch origin"
        local_sha = capture("cd #{oht_home}; git rev-parse master")
        remote_sha = capture("cd #{oht_home}; git rev-parse origin/master")
        if local_sha != remote_sha
        run "cd #{oht_home}; git pull; /opt/scripts/oht_version.sh --major"
        else
          puts "No changes - no tags!"
	end
      end

      desc "Update OHT Version (check)"
      task :check, :roles => :updater do
        puts "Check version" 
        run "cd #{oht_home}; git config --replace-all remote.origin.url #{repository}; git checkout master ; git fetch origin"
        local_sha = capture("cd #{oht_home}; git rev-parse master")
        remote_sha = capture("cd #{oht_home}; git rev-parse origin/master")
        if local_sha != remote_sha
        #run "cd #{oht_home}; git pull; /opt/scripts/oht_version.sh --major"
	  puts "You have changes, tag will be change!"
        else
          puts "No changes - no tags!"
        end
      end

  end

 ####### Sandbox ######## 
  namespace :sandbox do

      desc "Update sandbox from Git repository"
      task :git_update, :roles => :sandbox do
        puts "Updating sandbox"
        run "cd #{oht_home}; git config --replace-all remote.origin.url #{repository}; git checkout master; git reset --hard; git pull; git submodule update; composer install" 
        run "#{sudo} rm -rf /tmp/twig_cache"
        run "#{sudo} /opt/scripts/cron_builder.sh"
        run "killall php"
      end    

      desc "Update sandbox from Git repository and build frontend pages"
      task :git_update_build, :roles => :sandbox do
        puts "Updating sandbox"
        run "cd #{oht_home}; git config --replace-all remote.origin.url #{repository}; git checkout master; git reset --hard; git pull; git submodule update; composer install" 
        run "#{oht_home}/scripts/build_frontend_pages.sh"
        run "#{sudo} rm -rf /tmp/twig_cache"
        run "#{sudo} /opt/scripts/cron_builder.sh"
        run "killall php"
      end

        
    end 
 
  ####### Update Drupal server #######       
      
  namespace :drupal do

  desc "Update Drupal from Git repository"
    task :drupal_update, :roles => :drupal_server do
      puts "Updating Drupal site"     
      run "cd #{drupal_home};  git config --replace-all remote.origin.url #{repo_drupal}; git checkout master; git pull"
    end

  ####### Update OHT static version on Drupal server #######       
  

  desc "Update OHT static virsion on Drupal"
    task :oht_static_update, :roles => :drupal_server do
      puts "Updating OHT static version on Drupal site"
      run "ssh oht@www1 cat #{oht_home}/framework/Config/conf.ini | grep website.static.version > /home/oht/public_html/drupal/sites/default/version"
    end
  
    ####### Clear cache on Drupal server #######

  desc "Clear cache on Drupal server"
    task :clear_cache, :roles => :drupal_server do
      puts "Running drush cc all on Drupal server"
      run "cd /home/oht/public_html/drupal; drush cc all"
    end

  end
  
 ##### WebWordCount server ##### 
  namespace :webwordcount do
      
      desc "Update WebWordCount server"
      task :git_update, :roles => :wwc do
        puts "Updating WebWordCount"
        run "cd /home/ubuntu/wordcount ; git checkout master ; git pull"
      end    
    
    end    
      
         
 ####### Tester server ######   
  namespace :tester2 do      
  
    desc "Build OHT branch on tester2"
    task :build_branch, :roles => :tester2 do
      puts "Building branch '#{branch}'"      
      utilities.run_with_input "cd /home/oht/qa_script; ./oht-build #{branch}", /What do you want to do/
    end
    
    
    desc "Update testing frontend server(tester2)"
    task :git_update, :roles => :tester2 do
      puts "Updating branch '#{branch}' on testing frontend"
      run "cd public_html/#{branch}; git checkout #{branch}; git pull; git submodule update"
    end    
    
  end
 
 ###### Clean OHT aiCache ######
  namespace :aicache do
    desc "Clean aiCache after deploy"
    task :clean_aicache, :roles => :aicache do
       run "/opt/scripts/run_clean_www.sh"
    end
  end

 ###### Clean OWS aiCache ######
  namespace :aicache do
    desc "Clean aiCache after deploy"
    task :clean_aicache_ows, :roles => :aicache do
       run "/opt/scripts/run_clean_owsassets.sh"
    end
  end


 ###### Update OWS Assets Server ######
  namespace :ows do
    desc "Update any release and any branch on OWS Server"
    task :git_update_any, :roles => :ows do
      puts "Updating release '#{release}' branch '#{branch}'"
      run "cd public_html/ows/release/#{release}; git pull; git checkout #{branch}"
    end
  end

 ###### Update OWS 0.4 ######
  namespace :ows do
    desc "Update 0.4 release master branch on OWS Server"
    task :git_update, :roles => :ows do
	puts "Updating release 0.4 branch master"
	run "cd public_html/ows/release/0.4; git pull; git checkout master"
    end
  end

 ###### Deployment Marks to New Relic an HipChat ######
  namespace :deployment do
    desc "Add deployment to New Relic"
    task :newrelic do
        system("./nr_deployment.sh")
    end

    desc "Add deployment to New Relic with message"
    task :newrelic_with_message do
        system("./nr_deployment.sh #{message} ")
    end

    task :ows_hipchat do
        system('curl -sS -d "auth_token=22128c7e3dcfec45483ed2449f24de&room_id=104371&from=Capistrano&color=yellow&message_format=text&message=@all ohtjsframework deployed" https://api.hipchat.com/v1/rooms/message')
    end
  end

 ####### Clear Memcache #######       

  namespace :memcache do

  desc "Clear Memcache"
    task :clear_memcache, :roles => :memcached do
      puts "Clearing memcache"
      run "echo 'flush_all' | nc localhost 11211"
    end
  end

 desc "Parameter Testing"
   task :parameter do
     puts "Parameter test #{message}"
     system("./nr_deployment.sh 1 #{message}")
 end

 ###### Update oht-periodic #####
  namespace :oht_periodic do

  desc "Update oht-periodic"
    task :git_update_oht_periodic, :roles => :oht_periodic do
      puts "Updating oht-periodic"
      run "echo 'hello'"
    end
  end


##### Change Master Host to all Percona slaves #####

#  namespace :percona do
#	desc "Run CHANGE MASTER on all Percona slaves"
#	task :change_master, :roles => :mysql, :only => { :slave => true } do 
#		puts "Updating master host on all slaves"
#		run "#{sudo} mysql -e \"STOP SLAVE\""		
#		run "#{sudo} mysql -e \"CHANGE MASTER TO MASTER_HOST = '10.0.0.40', MASTER_USER = 'repl', MASTER_PORT =3306 \""
#		run "#{sudo} mysql -e \"START SLAVE\""
#	end
#end

#  ###### Update composer ######
#  desc "Update composer"
#  task :composer_update, :roles => [ :frontend_VPC, :backend, :sandbox ] do
#    puts "Updating composer build"
#    run "#{sudo} composer self-update"
#  end
  
    
  ###### Deploy OHT ####### 
  
  namespace :deploy do
  
    desc "Quick deploy OHT (for hotfixes w/o js)"
    task :hotfix do   
      version.patch
      frontend.git_update
      frontend.git_update_WEST
      backend.git_update
      sweatshop.git_update
      drupal.oht_static_update
      deploy.check_message
      sandbox.git_update
    end  
  
    ###### Deploy OHT and aiCache clean #######
  
    desc "Deploy OHT hotfix and clean aiCache"
    task :hotfix_clean do
      version.patch
      frontend.git_update
      frontend.git_update_WEST
      aicache.clean_aicache
      memcache.clear_memcache
      drupal.clear_cache
      backend.git_update
      sweatshop.git_update
      drupal.oht_static_update
      deploy.check_message
      sandbox.git_update
    end
  
    ###### Deploy OHT, build frontend pages and aiCache clean #######
  
    desc "Deploy OHT hotfix, build frontend pages and clean aiCache"
    task :hotfix_build_clean do
      version.patch
      frontend.git_update_build
      frontend.git_update_WEST
      aicache.clean_aicache
      memcache.clear_memcache
      drupal.clear_cache
      backend.git_update
      sweatshop.git_update
      drupal.oht_static_update
      deploy.check_message
      sandbox.git_update_build
    end
  

    ###### Deploy OHT, build frontend pages and aiCache clean #######

    desc "Deploy OHT release, build frontend pages and clean aiCache"
    task :release do
      version.minor
      frontend.git_update_build
      frontend.git_update_WEST
      aicache.clean_aicache
      memcache.clear_memcache
      drupal.clear_cache
      backend.git_update
      sweatshop.git_update
      drupal.oht_static_update
      deploy.check_message
      sandbox.git_update_build
    end
  
    ###### Deploy OHT, OWS and aiCache clean #######
  
    desc "Full deploy OHT & OWS and clean aiCache"
    task :release_ows do
      version.minor
      frontend.git_update_build
      frontend.git_update_WEST
      aicache.clean_aicache
      memcache.clear_memcache
      drupal.clear_cache
      backend.git_update
      sweatshop.git_update
      ows.git_update
      aicache.clean_aicache_ows
      drupal.oht_static_update
      deploy.check_message
      deployment.ows_hipchat
      sandbox.git_update_build
    end
  
  
    ###### Deploy OWS and aiCache clean #######
  
    desc "Full deploy OHT & OWS and clean aiCache"
    task :ows do
      ows.git_update
      aicache.clean_aicache_ows
      memcache.clear_memcache
      drupal.clear_cache
      deployment.ows_hipchat
    end

    ##### Yoni testing ########################

    desc "Decide if deploy message was given"
    task :check_message do
      if exists?(:message)
        puts "message exists"
        deployment.newrelic_with_message
      else
        puts "message does not exist"
        deployment.newrelic
      end
 
    end

  end
end
##### OTRS Server #####
#  namespace :otrs do
#    
#    desc "Update OTRS Server"
#    task :update_git, :roles => :otrs do
#      puts "Updating OTRS Server"
#      run "cd /opt/otrs_git/otrs; git checkout master; git pull; #{sudo} cp -r Custom/* /opt/otrs/Kernel/Output/HTML/Custom/; #{sudo} cp -r Modules/* /opt/otrs/Kernel/Modules"
#      run "#{sudo} /opt/otrs/bin/otrs.SetPermissions.pl --otrs-user=otrs --otrs-group=otrs --web-user=www-data --web-group=www-data /opt/otrs"
#    end
#      
#  end
