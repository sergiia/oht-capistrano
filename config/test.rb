 namespace :with_param do

     desc "run when param is set"
     task :with_param do
       puts "param set"
       #run "./nr_deployment.sh 1 #{message}"
	system("./nr_deployment.sh #{message} ")
     end

     desc "run when param is NOT set"
     task :without_param  do
       puts "NO PARAM"
        system("./nr_deployment.sh")
	#run "./nr_deployment.sh"
     end
end

  namespace :testing do

    desc "Test"
    task :checking do
      if exists?(:message)
	puts "exists"
	with_param.with_param
      else
	puts "does not exist"
	with_param.without_param
    end
end
end

