#!/bin/bash
#####  <<<<<<< WWW  >>>>>>		######
echo " Running WWW Roles "
/home/ubuntu/oht-capistrano/roles/www.sh
#
###### <<<<<< Drupal Servers >>>>>> #######
echo " Running Drupal Roles"
/home/ubuntu/oht-capistrano/roles/drupal.sh
#
###### <<<<<< UPDATER Servers >>>>>> #######
echo " Running UPDATER SERVERS "
#
/home/ubuntu/oht-capistrano/roles/update.sh
#
###### <<<<<< MEMCACHED SERVERS >>>>>> #######
echo " Running MEMCACHED SERVERS "
/home/ubuntu/oht-capistrano/roles/memcached.sh
#
###### <<<<<< Backend:Periodic >>>>>> #######
#
echo " Running  Backend:Periodic"
/home/ubuntu/oht-capistrano/roles/backend.periodic.sh
###### <<<<<< Backend:Sweatshop >>>>>> #######
#
echo " Running  Backend:Sweatshop"
/home/ubuntu/oht-capistrano/roles/backend.sweatshop.sh
#
echo " Running  OWSASSETS"
/home/ubuntu/oht-capistrano/roles/owsassets.sh
#
echo " Running TMSERVER_SANDBOX"
/home/ubuntu/oht-capistrano/roles/tmserver_sandbox.sh
#######   <<<<<<<< SANDBOX >>>>>>>>	#####
echo " Running SANDBOX"
/home/ubuntu/oht-capistrano/roles/sandbox.sh
#######   <<<<<<<<  WWC >>>>>>>>>> ####
echo " Running WWC "
/home/ubuntu/oht-capistrano/roles/wwc.sh
#
#######	  <<<<<<<   ELK  >>>>>>>>>> ####
echo " Running ELK "
/home/ubuntu/oht-capistrano/roles/elk.sh
#
######    <<<<<<<<<  RABBIT >>>>>>>>> ####
echo "Running Rabbit"
/home/ubuntu/oht-capistrano/roles/rabbitmq.sh
#
######    <<<<<<<<<   RABBIT >>>>>>>>> ####
echo "Running AICACHE"
/home/ubuntu/oht-capistrano/roles/aicache.sh
#
####### <<<<<<<<<<<< MONGO >>>>>>>>>>> #####
echo " Running MONGO"
/home/ubuntu/oht-capistrano/roles/mongo.sh


