#!/bin/bash
#
############# 			<<<<<<<<<<  AICACHE >>>>>>>>>>>>			#######
#
Instance_IDs=($(aws ec2 describe-instances --filters 'Name=tag:oht:environment,Values= production
' 'Name=tag:oht:stack,Values= it' 'Name=tag:oht:role,Values= aicache' 'Name=instance-state-name,Values=running' --output text --query 'Reservations[*].Instances[*].InstanceId'  --output text))

## Get Private_IP from InstanceID in AICACHE Instance
for i in "${Instance_IDs[@]}"
	do
		private_IP=`aws ec2 describe-instances --instance-ids $i --query 'Reservations[*].Instances[*].PrivateIpAddress' --output text`
		IPs=$IPs,"$private_IP"
	done
IP_format=`echo $IPs | sed 's/,/"/'|sed 's/,/","/g' | sed 's/$/"/'`
echo "Appending this format to Capistrano's ohtdeploy_vpn.rb under :role tag:"$IP_format
echo "present directory :-`pwd`"
## Deleting Old string with old IP address
sed -i "/role\ \:aicache/d" /home/ubuntu/oht-capistrano/config/servers.rb
#
## Write IP adress bellow ### Backend line
sed -i '/##AICACHE/a role\ :aicache,'$IP_format'' /home/ubuntu/oht-capistrano/config/servers.rb
echo " Roles AICACHE Successfully Add to Capistrano"