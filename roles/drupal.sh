#!/bin/bash
###### 						<<<<<	DRUPAL SERVERS   >>>>>			#######
#
Instance_IDs=($(aws ec2 describe-instances --filters 'Name=tag:oht:environment,Values= production
' 'Name=tag:oht:stack,Values= ohtweb' 'Name=tag:oht:role,Values= drupal' 'Name=instance-state-name,Values=running' --output text --query 'Reservations[*].Instances[*].InstanceId'  --output text))

## Get Private_IP from InstanceID in Drupal Instance
for i in "${Instance_IDs[@]}"
	do
		private_IP=`aws ec2 describe-instances --instance-ids $i --query 'Reservations[*].Instances[*].PrivateIpAddress' --output text`
		IPs=$IPs,"$private_IP"
	done
IP_format=`echo $IPs | sed 's/,/"/'|sed 's/,/","/g' | sed 's/$/"/'`
echo "Appending this format to Capistrano's ohtdeploy_vpn.rb under :role tag:"$IP_format
## Deleting Old string with old IP address
sed -i "/role\ \:drupal_server/d" /home/ubuntu/oht-capistrano/config/servers.rb
#
## Write IP adress bellow ### Drupal line
sed -i '/##DRUPAL/a role\ :drupal_server,'$IP_format'' /home/ubuntu/oht-capistrano/config/servers.rb
echo " Roles DRUPAL Successfully Add to Capistrano"
#
